## [jc141 - setup guide](https://gitlab.com/jc141x/setup)


Everything you need to do before playing our releases.

<br>

## [jc141 - matrix](https://matrix.to/#/#rumpowered:matrix.org)
Room address: #rumpowered:matrix.org


For technical support, come here.


This is a non-profit project. We do not guarantee that everything can be fixed or will be perfect.

<br>

## [Freedom to Torrent](freedom-to-torrent.md)

This is a guide to avoid DMCA claims without payment. As well as settings to prevent the leaking of your IP.

<br>

## [jc141 - 1337x.to](https://1337x.to/user/johncena141/)


Browse through our releases on a torrent tracker. (Unusable without uBlock Origin)


Pay attention to the domain; there are many copycats.


```
1337x.to 1337x.st x1337x.ws x1337x.eu x1337x.se 1337x.is 1337x.gd
```


- These are the official 1337x domains. If you came from one with a different domain, then it's fake, and we won't provide support for anything downloaded from there.
- If your ISP has blocked 1337x, then use [Tor](https://www.torproject.org/), a VPN, or a trusted public proxy. Read more: https://1337x-proxy.github.io/

<br>

## [jc141 - scripting](https://gitlab.com/jc141x/scripting/)


Donations - Monero: 4ABGQLAeAgiauvay11VRrWXRRtraRCU6oaC6uG9RUnNCHN4eepzWjEB6sHF92sUrSED5b8GyY7Ayh57R1jUdcKZg7is2DW3

![](monero.png)
