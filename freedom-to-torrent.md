## Freedom to Torrent - A guide for newbies... who can't afford paid VPNs.


### Riseup VPN

Depending on where you live, you might be under the surveillance of a corporate-aligned government. Meaning the interests of the corporate world by far surpass the government's willingness to protect its citizens.


As a result, torrenting content might result in receiving DMCA claims.


We suggest using the [RiseupVPN](https://riseup.net/en/vpn) since it is a free, no-account-required, funded-by-donations service that doesn't track users.


However, we consider the official client provided by RiseupVPN pretty unreliable as it disconnects easily.


Alternatively, you could use an ovpn.conf file by adding it to your Network Manager. We've added [the config file](riseup-ovpn.conf ) to this repo, or you could generate one yourself with this [script](https://github.com/BarbossHack/RiseupVPN-OpenVPN).


<br><br>


### Preventing the leaking of your IP


Any VPN that you might use could at any point disconnect and reveal your real IP while torrenting. This can be fixed.


<br><br>


#### qBittorrent settings


Go to Preferences - Advanced and set Network Interfaces to tun0 or wg if you used WireGuard instead.


This will prevent the client from using your network without it being connected to a VPN.


However, this may not be fail-proof since it is a configuration in the user space that can be changed without admin privileges.


<br><br>


#### Creating a VPN tunnel with ufw (for OpenVPN)


This method prevents your GNU/Linux OS from using the network without a VPN connection. This can be disabled only with Admin privileges.


<br>


First, install the ufw package on your system.


<br>


##### Enable ufw (firewall) with vpn


###### Default policies


```
sudo ufw default deny incoming

sudo ufw default deny outgoing

```


###### Openvpn interface (adjust interface accordingly to your configuration)


```
sudo ufw allow out on tun0

```


###### Connection to the OpenVPN server itself (adjust port accordingly to your configuration)


```
sudo ufw allow out to any port 1194

```


###### Local Network (allow outgoing to all private networks)


```
sudo ufw allow out to 10.0.0.0/8

sudo ufw allow out to 172.16.0.0/12

sudo ufw allow out to 192.168.1.0/24

```


###### DNS (need to get openvpn server ip address and so always allowed)


```
sudo ufw allow out from any to any port 53

```

###### Finally enable the firewall


```
sudo ufw enable

```

<br><br><br><br>


###### Reverting the ufw settings

In case you need to access the internet again but without the VPN, run these commands:


```
sudo ufw --force disable

sudo ufw --force reset

sudo ufw default deny incoming
sudo ufw default allow outgoing

sudo ufw --force enable
```

If you are running these commands on a device on which you are connected using ssh, make sure you have alternative means of accessing the device in case you are locked out by ufw by any misconfiguration.